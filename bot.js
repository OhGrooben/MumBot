const Discord = require("discord.js");
const client = new Discord.Client();

let config;

try {
  config = require("./config.json");
  
}

catch(error) {
  Console.log("There was an issue finding the config.json file, if you have not yet created one, please check the README for more info.");
}

console.log("Do I get here?");

client.on("ready", () => {
  console.log("I am ready!");
});

client.on("message", (message) => {
  if (message.content.startsWith("ping")) {
    message.channel.send("pong!");
  }
});

client.on("message", (message) => {
  if (message.content.includes(", I'm Dad!")) {
    message.channel.send("Shut up Harold!");
  }
});

client.login(config.token);

