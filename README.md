MumBot
# Introduction
This is to annoy Dadbot, nothing else.

# I need a config.json file!?
Yes, you do. The config.json file contains a desired prefix for any commands, as well as the all important bot token!
```json
{
    "token": "Your token here!",
    "prefix": "__"
}
```